use std::{
    hash::{BuildHasher, Hash},
    sync::Arc,
};

use crate::common::concurrent::arc::MiniArc;

use parking_lot::{Mutex, MutexGuard};

type LockMap<K, S> = papaya::HashMap<Arc<K>, MiniArc<Mutex<()>>, S>;

// We need the `where` clause here because of the Drop impl.
pub(crate) struct KeyLock<'a, K, S>
where
    K: Eq + Hash,
    S: BuildHasher,
{
    map: &'a LockMap<K, S>,
    key: Arc<K>,
    lock: MiniArc<Mutex<()>>,
}

impl<K, S> Drop for KeyLock<'_, K, S>
where
    K: Eq + Hash,
    S: BuildHasher,
{
    fn drop(&mut self) {
        if MiniArc::count(&self.lock) <= 2 {
            let _ = self
                .map
                .pin()
                .remove_if(&self.key, |_k, v| MiniArc::count(v) <= 2);
        }
    }
}

impl<'a, K, S> KeyLock<'a, K, S>
where
    K: Eq + Hash,
    S: BuildHasher,
{
    fn new(map: &'a LockMap<K, S>, key: &Arc<K>, lock: MiniArc<Mutex<()>>) -> Self {
        Self {
            map,
            key: Arc::clone(key),
            lock,
        }
    }

    pub(crate) fn lock(&self) -> MutexGuard<'_, ()> {
        self.lock.lock()
    }
}

pub(crate) struct KeyLockMap<K, S> {
    locks: LockMap<K, S>,
}

impl<K, S> KeyLockMap<K, S>
where
    K: Eq + Hash,
    S: BuildHasher,
{
    pub(crate) fn with_hasher(hasher: S) -> Self {
        let locks = papaya::HashMap::builder()
            .hasher(hasher)
            // .resize_mode(papaya::ResizeMode::Blocking)
            .build();

        Self { locks }
    }

    pub(crate) fn key_lock(&self, key: &Arc<K>) -> KeyLock<'_, K, S> {
        let kl = MiniArc::new(Mutex::new(()));
        match self.locks.pin().try_insert(Arc::clone(key), kl.clone()) {
            Ok(_) => KeyLock::new(&self.locks, key, kl),
            Err(err) => KeyLock::new(&self.locks, key, MiniArc::clone(err.current)),
        }
    }
}

#[cfg(test)]
impl<K, S> KeyLockMap<K, S>
where
    K: Eq + Hash,
    S: BuildHasher,
{
    pub(crate) fn is_empty(&self) -> bool {
        self.locks.pin().len() == 0
    }
}
